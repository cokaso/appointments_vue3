const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors')

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        "./resources/**/*.vue",
    ],

    theme: {
        extend: {
            colors: {
                // Build your palette here
                gray: colors.neutral,
                red: colors.red,
                blue: colors.sky,
                yellow: colors.yellow,
                orange:colors.orange,
                lime: colors.lime,
                emerald: colors.emerald,
                teal:colors.teal,
                cyan: colors.cyan,
                lightBlue: colors.sky,
                purple: colors.purple,
                rose: colors.rose,

                primary: '#0A5F46',  // Demo
                secondary: '#50B946',// Demo

            },
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
