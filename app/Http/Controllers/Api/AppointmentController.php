<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentRequest;
use App\Http\Resources\AppointmentResource;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\ListAppointmentsResource;
use App\Models\Appointment;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ListAppointmentsResource::collection(Appointment::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppointmentRequest $request)
    {

        $data = $request->validated();

        $dateStart = (new Carbon($data["date_start"]))->minute(0)->second(0)->format("Y-m-d H:i");
        $dateEnd  = (new Carbon($dateStart))->minute(59)->second(59)->format("Y-m-d H:i");

        $appointmentRegistered = Appointment::where("date_start", ">=", $dateStart)
                                 ->where("date_end", "<=", $dateEnd)
                                 ->where("enabled",true)->first();

        if(isset($appointmentRegistered)){
            $result = new \stdClass();
            $result->valid   = false;
            $result->message = "No se puede registrar en fecha $dateStart, debe seleccionar otra fecha y/u hora.";
            $result->errors  = array("$result->message");
            return new ErrorResource($result);
        }


        $data['user_id']    = (!isset($data['user_id']))?1:$data['user_id'];
        $data['date_start'] = $dateStart;
        $data['date_end']   = $dateEnd;
        $data['state']      = Appointment::STATE_PENDING;
        $data['enabled']    = true;

        $appointment = Appointment::create($data);
        $appointment->valid   = true;
        $appointment->message = "Registro correcto";

        return new AppointmentResource($appointment);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        return new AppointmentResource($appointment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(AppointmentRequest $request, Appointment $appointment)
    {
        $data = $request->validated();
        $appointment->update();
        return new AppointmentResource($appointment);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
        $appointment->delete();
        return response()->noContent();

    }
}
