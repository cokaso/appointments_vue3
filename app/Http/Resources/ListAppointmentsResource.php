<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ListAppointmentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $date_start = strlen($this->date_start)>2 ? (new Carbon($this->date_start))->format("Y-m-d H:i"):null;
        return [
            'id'         => $this->id,
            'user_id'    => $this->user_id,
            'usuario'    => $this->user->email,
            'description'=> $this->description,
            'date_start' => $date_start,
            'state'      => $this->state,
        ];
    }
}
