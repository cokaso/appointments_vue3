<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceResponse;

class ErrorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "valid"   => $this->valid,
            "message" => $this->message,
        ];
    }

    /*public function toResponse($request)
    {
        return (new ResourceResponse($this))->toResponse($request)->setStatusCode(422);
    }*/
}
