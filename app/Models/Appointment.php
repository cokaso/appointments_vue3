<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Appointment extends Model
{
    use HasFactory;


    protected $fillable = ['user_id','description','date_start','date_end','address',
                           'state','enabled','created_by','updated_by'];

    public const STATE_PENDING = "pending";
    public const STATE_APPROVED = "approved";
    public const STATE_CANCELLED = "cancelled";


    public function user():BelongsTo {
        return $this->belongsTo(User::class);
    }

    public static function getStates():array{
        return [self::STATE_PENDING,self::STATE_APPROVED,self::STATE_CANCELLED];
    }

}
