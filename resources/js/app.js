import './bootstrap';

import Alpine from 'alpinejs';
window.Alpine = Alpine;
Alpine.start();

import { createApp } from "vue";
import router from './router';
import App from './pages/App.vue';

import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css';
import Notifications from '@kyvg/vue3-notification';

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';


const app = createApp(App)
app.$user = window.User
app.use(router)
app.use(VueSweetalert2)
app.component('Datepicker', Datepicker)
app.component('Notifications', Notifications)

window.Swal =  app.config.globalProperties.$swal;

app.mount("#app")

