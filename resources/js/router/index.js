import { createRouter, createWebHistory } from 'vue-router';

import CalendarAppointments from "../components/appointments/CalendarAppointments.vue";
import AppointmentsIndex from '../components/appointments/AppointmentsIndex.vue';
import AppointmentCreate from "../components/appointments/AppointmentCreate.vue";
import AppointmentEdit   from "../components/appointments/AppointmentEdit.vue";

const routes = [
    {
        path: '/dashboard',
        name: 'appointments.index',
        component: CalendarAppointments
    },
    {
        path: '/appointments/create',
        name: 'appointments.create',
        component: AppointmentCreate
    },
    {
        path: '/appointments/:id/edit',
        name: 'appointments.edit',
        component: AppointmentEdit,
        props: true
    },

    {
        path: '/admin/appointments',
        name: 'admin-appointments.index',
        component: AppointmentsIndex
    },

];

export default createRouter({
    history:createWebHistory(),
    routes
})
