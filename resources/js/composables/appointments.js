import {ref} from 'vue'
import axios from 'axios'
import { useRouter } from 'vue-router'
import { notify } from "@kyvg/vue3-notification";


export default function useAppointments(){

    const appointments = ref([])
    const appointment = ref([])
    const router = useRouter()
    const errors = ref('')
    const status = ref('')

    const getAppointments = async () => {
        let response = await axios.get('/api/appointments')
        appointments.value = response.data.data
    }

    const getAppointment = async (id) => {
        let response = await axios.get('/api/appointments/'+id)
        appointment.value = response.data.data
    }

    const storeAppointment = async (data) => {
        errors.value = ''
        try {

            let response = await axios.post('/api/appointments',data)
            if(Boolean(response.data.data.valid)){
                 await router.push({name:'appointments.index'})
            }else{
                status.value = response.data.data.message
                return alert(response.data.data.message)
            }

        }catch (e) {

            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const updateAppointment = async (id) => {
        errors.value = ''
        try {
            await axios.put('/api/appointments/' + id, appointment.value)
            await router.push({name: 'appointments.index'})
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const destroyAppointment = async (id) => {
        let response = await  axios.delete('/api/appointments/'+id)

    }

    const can = (permissionName) =>{
        let permissions = window.Laravel.jsPermissions.permissions
        return permissions.indexOf(permissionName) !== -1;
    }

    return {
        appointments,
        appointment,
        errors,
        status,
        getAppointments,
        getAppointment,
        storeAppointment,
        updateAppointment,
        destroyAppointment,
        can
    }

}
