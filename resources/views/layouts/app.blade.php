<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if (Auth::check())
            <meta name="user_id" content="{{ Auth::user()->id }}" />
        @endif
        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Scripts -->
        @vite('resources/css/app.css')
    </head>
    <body class="font-sans antialiased">
        @include("layouts.navigation")
        <div id="app" class="min-h-screen bg-gray-200 px-2"></div>

        <script>

            window.User = {!! json_encode(optional(auth()->user())->only('id', 'is_admin')) !!}

            window.Laravel = {
                csrfToken: "{{ csrf_token() }}",
                jsPermissions: {!! auth()->check()?auth()->user()->jsPermissions():0 !!}
            }


        </script>

        @vite('resources/js/app.js')

    </body>
</html>
