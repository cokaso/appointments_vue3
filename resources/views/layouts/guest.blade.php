<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body>
        <div class="font-sans text-gray-900 antialiased">
            {{ $slot }}
            <div class="text-center p-3 bg-blue-500 sticky bottom-0 z-50">
                <p class="text-white cursor-hand">
                    Back-end Developer <a href="mailto:jperez.erpnitro@gmail.com" target="_blank" class="font-bold hover:text-pink-500">jperez.erpnitro@gmail.com</a> |
                    <a href="https://www.linkedin.com/in/cokaso/" target="_blank" class="font-bold hover:text-pink-500">https://www.linkedin.com/in/cokaso/</a>
                </p>
            </div>
        </div>
    </body>
</html>
