<?php

namespace Database\Factories;

use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Appointment>
 */
class AppointmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $date_start = Carbon::now();
        $date_end   = Carbon::now()->addHours(1);

        return [
            'date_start' => $date_start,
            'date_end'   => $date_end,
            'state'      => $this->faker->randomElement(Appointment::getStates()),
            'address'    => $this->faker->address,
            'description'=> $this->faker->sentence
        ];
    }
}
