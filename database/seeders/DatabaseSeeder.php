<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Appointment;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BaseSeeder::class);

        $password="123456";

        $user = User::create(['is_admin'=>1,'name'=>'Admin', 'email'=>'admin@admin.com','password'  =>bcrypt($password)]);
        $user->assignRole('admin');

        $user = User::create(['name'=>'Invitado', 'email'=>'user@user.com','password'  =>bcrypt($password)]);
        $user->assignRole('invitado');

        /*
                $users = User::all();
                $appointments = Appointment::factory(10)->create([
                    'user_id'=> $users->random()->id
                ]);*/

    }
}
