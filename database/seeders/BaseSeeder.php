<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // ERP ADMIN create permissions

        Permission::create(['name' => 'root_config']);

        Permission::create(['name' => 'ver-users']);
        Permission::create(['name' => 'crear-users']);
        Permission::create(['name' => 'editar-users']);

        Permission::create(['name' => 'ver-roles']);
        Permission::create(['name' => 'crear-roles']);
        Permission::create(['name' => 'editar-roles']);

        Permission::create(['name' => 'ver-permisos']);
        Permission::create(['name' => 'crear-permisos']);
        Permission::create(['name' => 'editar-permisos']);

        // AGENDA DE CITAS

        Permission::create(['name' => 'ver-citas']);
        Permission::create(['name' => 'solicitar-citas']);

        Permission::create(['name' => 'crear-citas']);
        Permission::create(['name' => 'editar-citas']);
        Permission::create(['name' => 'eliminar-citas']);


        // ERP SUPER_ADMIN ROLE

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());


        $role_invitado = Role::create(['name' => 'invitado'])
                        ->givePermissionTo(['ver-citas','solicitar-citas']);


    }
}
